package com.hl.glidedemo.bitmaptarget

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import com.bumptech.glide.request.Request
import com.bumptech.glide.request.target.SizeReadyCallback
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition

class BitmapTarget(private val callback: (Bitmap?) -> Unit) : Target<Bitmap>{
    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
        callback(resource)
    }

    // 可选：如果需要在加载失败时收到通知，可以重写以下方法
    override fun onLoadFailed(errorDrawable: Drawable?) {
        callback(null) // 或者其他失败处理逻辑
    }

    override fun onLoadCleared(placeholder: Drawable?) {}
    override fun getSize(cb: SizeReadyCallback) {
        TODO("Not yet implemented")
    }

    override fun removeCallback(cb: SizeReadyCallback) {
        TODO("Not yet implemented")
    }

    override fun setRequest(request: Request?) {
        TODO("Not yet implemented")
    }

    override fun getRequest(): Request? {
        TODO("Not yet implemented")
    }

    override fun onStart() {
        TODO("Not yet implemented")
    }

    override fun onStop() {
        TODO("Not yet implemented")
    }

    override fun onDestroy() {
        TODO("Not yet implemented")
    }

    override fun onLoadStarted(placeholder: Drawable?) {
        TODO("Not yet implemented")
    }


}