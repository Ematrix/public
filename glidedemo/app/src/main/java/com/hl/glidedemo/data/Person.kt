package com.hl.glidedemo.data

data class Person(
    var name :String,
    var sex :String,
    var adress :String,
    var age : Int
)