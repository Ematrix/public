package com.hl.glidedemo.ui

import android.os.Bundle
import android.os.CountDownTimer
import com.hl.glidedemo.databinding.ActivityAboutBinding
import com.hl.lib.base.BaseActivityB
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

class AboutActivity(millisInFuture: Long) : BaseActivityB<ActivityAboutBinding>() {
    override fun initData() {
        TODO("Not yet implemented")
    }

    override fun initView() {
        TODO("Not yet implemented")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val millisInFuture = 1000L
        val countDownInterval = 10L
        val timer = object : CountDownTimer(millisInFuture, countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {
                TODO("Not yet implemented")
            }

            override fun onFinish() {
                TODO("Not yet implemented")

            }
        }

    }

    override fun onNetStateChanged(connected: Boolean) {
        TODO("Not yet implemented")
    }

    suspend fun countDown(
        durationInSeconds: Int,
        onTick: (remainingTime: Int) -> Unit,
        onFinish: () -> Unit
    ) = withContext(Dispatchers.Main) {
        var remainingTime = durationInSeconds
        while (remainingTime > 0) {
            delay(1000L) // 每隔一秒执行一次
            remainingTime--
            onTick(remainingTime) // 更新 UI 显示剩余时间
        }
        onFinish() // 倒计时结束时调用
    }

    override fun provideViewBinding(): ActivityAboutBinding {
        return ActivityAboutBinding.inflate(layoutInflater)
    }
}