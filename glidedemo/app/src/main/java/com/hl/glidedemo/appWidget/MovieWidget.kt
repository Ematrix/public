package com.hl.glidedemo.appWidget

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.Log
import android.widget.RemoteViews
import androidx.annotation.RequiresApi
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.hl.glidedemo.R
import com.hl.glidedemo.data.const.UrlConst
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import java.util.Calendar


/**
 * Implementation of App Widget functionality.
 */
class MovieWidget : AppWidgetProvider() {
    private var scope = CoroutineScope(Dispatchers.Main)

    companion object {
        private const val TAG = "MovieWidget"
        private const val ACTION_CHANGE_TIME = "ACTION_CHANGE_TIME"

        private val resList = listOf(
            R.drawable.ic_launcher_background,
            R.drawable.ic_launcher_background1,
            R.drawable.ic_launcher_background2,
            R.drawable.ic_launcher_background3,
        )

        private val picUrlList = listOf(
            UrlConst.imageUrl1,
            UrlConst.imageUrl2,
            UrlConst.imageUrl3,
            UrlConst.imageUrl4,
        )
        private var index = 0

        // 取消的时候要用
        private var pendingIntent: PendingIntent? = null
        var alarmManager: AlarmManager? = null
    }

    override fun onReceive(context: Context, intent: Intent?) {
        initIntent(context)
        intent?.let {
            Log.e(TAG, "onReceive action = ${it.action}")
            when (it.action) {
                ACTION_CHANGE_TIME -> showNextPicture(context)
            }
        }
        super.onReceive(context, intent)
    }

    private fun showNextPicture(context: Context) {
        if (index == resList.size - 1) {
            index = 0
        } else {
            index++
        }
        Log.e(TAG, "showNextPicture: index = $index")
        val views = RemoteViews(context.packageName, R.layout.movie_widget)
        views.setDisplayedChild(R.id.viewFlipper, index)
        val appWidgetManager = AppWidgetManager.getInstance(context)
        val appIds =
            appWidgetManager.getAppWidgetIds(ComponentName(context, MovieWidget::class.java))
        appWidgetManager.updateAppWidget(appIds, views)

        setChangePictureAlarm(context)
    }

    override fun onUpdate(
        context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray
    ) {
        // 更新每个 AppWidget
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }


    private fun updateAppWidget(
        context: Context, appWidgetManager: AppWidgetManager, appWidgetId: Int
    ) {
        Log.e(TAG, "updateAppWidget Enter")

        // 获取 RemoteViews 实例
        val views = RemoteViews(context.packageName, R.layout.movie_widget)
        val subViews = RemoteViews(context.packageName, R.layout.movie_widget_item)
        views.removeAllViews(R.id.viewFlipper);
        for (i in 0..3) {
            Log.e(TAG, "updateAppWidget: i = $i")
            subViews.setImageViewResource(R.id.imageView, resList[i])
            views.addStableView(R.id.viewFlipper, subViews,i)
//            views.setDisplayedChild(R.id.viewFlipper, 0)
//            appWidgetManager.updateAppWidget(appWidgetId, views)
//            Thread.sleep(1000)
            Log.e(TAG, "updateAppWidget  loadEnd : i = $i")
        }
        Log.e(TAG, "updateAppWidget: 全部加载完成")
        index = 0
        views.setDisplayedChild(R.id.viewFlipper, 1)
        // 通知 AppWidgetManager 更新 AppWidget
        appWidgetManager.updateAppWidget(appWidgetId, views)
        setChangePictureAlarm(context)
    }



//    private fun updateAppWidget(
//        context: Context, appWidgetManager: AppWidgetManager, appWidgetId: Int
//    ) {
//        val views = RemoteViews(context.packageName, R.layout.movie_widget)
//        val subViews = RemoteViews(context.packageName, R.layout.movie_widget_item)
//
//        index = 0
//        scope.launch {
//            for (i in 0..3) {
//                loadSinglePic(context, i, subViews, views, appWidgetManager, appWidgetId)
//            }
//            Log.e(TAG, "updateAppWidget: 全部图片加载完成")
//            views.setDisplayedChild(R.id.viewFlipper, 0)
//            appWidgetManager.updateAppWidget(appWidgetId, views)
//            setChangePictureAlarm(context)
//        }
//    }

    private suspend fun loadSinglePic(
        context: Context,
        i: Int,
        subViews: RemoteViews,
        views: RemoteViews,
        appWidgetManager: AppWidgetManager,
        appWidgetId: Int
    ): Int {
        return suspendCancellableCoroutine {
            Log.e(TAG, "updateAppWidget: i = $i")
            val opts = RequestOptions()
                .format(DecodeFormat.PREFER_RGB_565)
                .override(200)
              .diskCacheStrategy(DiskCacheStrategy.NONE)
            Glide.with(context).asBitmap().apply(opts).load(picUrlList[i])
                .into(object : CustomTarget<Bitmap?>() {
                    @RequiresApi(Build.VERSION_CODES.S)
                    override fun onResourceReady(
                        resource: Bitmap, transition: Transition<in Bitmap?>?
                    ) {
                        try {
                            Log.e(TAG, "onResourceReady: 图片准备好,$i,$resource")
                            subViews.setImageViewBitmap(R.id.imageView, resource)
                            views.addView(R.id.viewFlipper, subViews)
                            appWidgetManager.updateAppWidget(appWidgetId, views)
                            it.resumeWith(Result.success(1))
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {
                        it.resumeWith(Result.success(2))
                    }
                })
        }
    }


    @SuppressLint("ScheduleExactAlarm")
    fun setChangePictureAlarm(context: Context) {

        // 获取AlarmManager服务
        if (alarmManager == null) {
            alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        }

        pendingIntent = initIntent(context)
        pendingIntent?.let {

            // 设置闹钟触发时间
            val calendar: Calendar = Calendar.getInstance()
            calendar.add(Calendar.SECOND, 5) // 添加5秒到当前时间作为闹钟触发时间
            alarmManager?.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), it)
        }

    }

    private fun initIntent(context: Context): PendingIntent {
        // 创建一个意图(PendingIntent)，这个意图将在闹钟触发时被发送出去
        val intent = Intent(context, MovieWidget::class.java).apply {
            action = ACTION_CHANGE_TIME
        }

        return PendingIntent.getBroadcast(
            context, 30, intent, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
        )
    }

    override fun onDeleted(context: Context?, appWidgetIds: IntArray?) {
        pendingIntent?.takeIf { alarmManager != null }.let {
            alarmManager?.cancel(it!!)
        }
        super.onDeleted(context, appWidgetIds)
    }


}
