package com.hl.glidedemo.worker

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MyWork2(context: Context, workerParams: WorkerParameters) : Worker(context, workerParams) {

    companion object {
        private const val TAG = "MyWork2"
        private const val imageUrl =
            "https://www.25h.net/d/file/20230115/b1dd4ed72f4ebdb7a9c5dade1df5a08d.jpg"
        private const val imageUrl2 =
            "https://file1.shop265.com/tk/20200809/02330d1ddcc43d76b78aeb544f69ea48.jpg"
    }

    val coroutineScope = CoroutineScope(Dispatchers.Main)


    override fun doWork(): Result {
        var bitmap1: Bitmap? = null
        var bitmap2: Bitmap? = null

        // 方法一 : 会先返回,因为在协程调度的过程中,还是会走下面的代码的,
        // 注意对协程概念的理解
        coroutineScope.launch {
            val job = coroutineScope.launch(Dispatchers.IO){
                bitmap1 = loadImage(imageUrl)
                Log.e(TAG, "doWork2: 得到 bitmap1  = $bitmap1")
                bitmap2 = loadImage(imageUrl2)
                Log.e(TAG, "doWork2: 得到 bitmap2  = $bitmap2")
            }
            job.join()

            Log.e(TAG, "doWork2: 两个bitmap都已获得, $bitmap1,")
            Log.e(TAG, "doWork2: 两个bitmap都已获得, $bitmap2,")
        }

        //方法二 先打印图片的的信息,再返回,这是因为 runBlocking 是阻塞式的
        //注意方法一和方法二的区别
//        runBlocking {
//            bitmap1 = loadImage(imageUrl)
//            Log.e(TAG, "doWork2: 得到 bitmap1  = $bitmap1")
//            bitmap2 = loadImage(imageUrl2)
//            Log.e(TAG, "doWork2: 得到 bitmap2  = $bitmap2")
//
//            Log.e(TAG, "doWork2: 两个bitmap都已获得, $bitmap1,")
//            Log.e(TAG, "doWork2: 两个bitmap都已获得, $bitmap2,")
//
//        }



        return Result.success()
    }

    private suspend fun loadImage(url: String): Bitmap? {
        return withContext(Dispatchers.IO) {
            val deferred = CompletableDeferred<Bitmap?>()
            Glide.with(applicationContext)
                .asBitmap()
                .load(url)
                .override(50, 50)
                .into(object : CustomTarget<Bitmap?>() {
                    override fun onLoadFailed(errorDrawable: Drawable?) {
                        deferred.completeExceptionally(Exception("Glide load failed"))
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {
                        TODO("Not yet implemented")
                    }

                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: Transition<in Bitmap?>?
                    ) {
                        deferred.complete(resource)
                    }
                })
            deferred.await()
        }
    }
}