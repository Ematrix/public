package com.hl.glidedemo.composeui

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp

class ComposeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            showUI()
        }
    }

    @Composable
    @Preview
    private fun showUI() {
        Surface(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
                .background(Color.Black),
            shape = RoundedCornerShape(Dp(18f))

        ) {
            Column(
                modifier = Modifier
                    .padding(Dp(50f))
                    .width(Dp(100f))
                    .padding(Dp(0f)),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally



            ) {
                Row(
                    modifier = Modifier.padding(Dp(5f)),
                    horizontalArrangement = Arrangement.Center,//设置水平居中对齐
                    verticalAlignment = Alignment.CenterVertically//设置垂直居中对齐
                ) {
                    Text(
                        modifier = Modifier
                            .padding(Dp(10f))
                            .background(Color.Red),
                        text = "我来了,你在哪里"
                    )

                    Text(
                        modifier = Modifier
                            .fillMaxWidth()
                            .background(Color.Cyan),
                        textAlign = TextAlign.Center,

                        text = "中华人民共和国"
                    )
                }

                Row (
                    modifier = Modifier.padding(Dp(5f)),
                    horizontalArrangement = Arrangement.Center,//设置水平居中对齐
                    verticalAlignment = Alignment.CenterVertically//设置垂直居中对齐
                ){
                    Button(
                        onClick = {},
                        modifier = Modifier.background(Color.Red)
                    ) {
                        Text("我来了,你在哪里")
                    }

                    Button(
                        onClick = {},
                        modifier = Modifier
                            .fillMaxWidth()
                            .background(Color.Cyan),
                    ) {
                        Text("中华人民共和国")
                    }
                }
            }


        }

    }
}