package com.hl.glidedemo.appWidget

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import com.hl.glidedemo.R
import com.hl.glidedemo.data.const.UrlConst

class MyWidgetService : RemoteViewsService() {
    override fun onGetViewFactory(intent: Intent): RemoteViewsService.RemoteViewsFactory {
        return MyWidgetFactory(this.applicationContext, intent)
    }

    class MyWidgetFactory(private val context: Context, private val intent: Intent) :
        RemoteViewsService.RemoteViewsFactory {
        // 假设你有一个数据源，例如一个图片列表
        private val imageUrls: List<String> =
            listOf(UrlConst.imageUrl1, UrlConst.imageUrl2, UrlConst.imageUrl3)

        override fun onCreate() {
            TODO("Not yet implemented")
        }

        override fun onDataSetChanged() {
            TODO("Not yet implemented")
        }

        override fun onDestroy() {
            TODO("Not yet implemented")
        }


        override fun getCount(): Int {
            return imageUrls.size
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getViewAt(position: Int): RemoteViews {
            val subViews = RemoteViews(context.packageName, R.layout.movie_widget_item)
            val imageUrl = imageUrls[position]
            subViews.setImageViewResource(R.id.imageView,R.drawable.ic_launcher_background)


//            val pendingIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
//                PendingIntent.getBroadcast(
//                    context,
//                    300,
//                    changeIntent,
//                    PendingIntent.FLAG_IMMUTABLE
//                )
//            } else {
//                PendingIntent.getBroadcast(
//                    context,
//                    300,
//                    changeIntent,
//                    PendingIntent.FLAG_ONE_SHOT or PendingIntent.FLAG_IMMUTABLE
//                )
//            }
//            views.setPendingIntentTemplate(android.R.id.text1, pendingIntent)
            return subViews
        }

        override fun getLoadingView(): RemoteViews? {
            return null
        }

        override fun getViewTypeCount(): Int {
            return 1
        }

        override fun hasStableIds(): Boolean {
            return true
        }

        // 其他必要的方法...
    }
}