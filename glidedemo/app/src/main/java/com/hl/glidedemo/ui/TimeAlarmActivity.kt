package com.hl.glidedemo.ui

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import com.hl.glidedemo.databinding.ActivityTimeAlarmBinding
import com.hl.lib.base.BaseActivityBM
import com.hl.lib.utils.AlarmUtils
import com.hl.lib.utils.DataStorePreferences
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class TimeAlarmActivity :
    BaseActivityBM<ActivityTimeAlarmBinding, TimeAlarmActivityViewModel>() {

    companion object {
        const val REQUEST_CODE_WORK = 200
        const val REQUEST_CODE_REST = 300

        const val SP_KEY_WORK = "work_gap"
        const val SP_KEY_REST = "rest_gap"
    }

    private var workPendingIntent: PendingIntent? = null
    private var restPendingIntent: PendingIntent? = null
    override val mViewModel: TimeAlarmActivityViewModel by viewModels()

    var alarmManager: AlarmManager? = null

    override fun provideViewBinding(): ActivityTimeAlarmBinding {
        return ActivityTimeAlarmBinding.inflate(layoutInflater)
    }

    override fun initData() {
        alarmManager = mContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    }

    override fun initView() {
        mBinding.btnTimeWorkAdd.setOnClickListener {
            mViewModel.workMinute = doAdd(mViewModel.workMinute)
            mBinding.etTimeWorkMinute.setText(mViewModel.workMinute.toString())
        }

        mBinding.btnTimeWorkMinus.setOnClickListener {
            mViewModel.workMinute = doMinus(mViewModel.workMinute)
            mBinding.etTimeWorkMinute.setText(mViewModel.workMinute.toString())
        }

        mBinding.btnTimeWorkAddSecond.setOnClickListener {
            mViewModel.workSecond = doAdd(mViewModel.workSecond)
            mBinding.etTimeWorkSecond.setText(mViewModel.workSecond.toString())
        }

        mBinding.btnTimeWorkMinusSecond.setOnClickListener {
            mViewModel.workSecond = doMinus(mViewModel.workSecond)
            mBinding.etTimeWorkSecond.setText(mViewModel.workSecond.toString())
        }

        mBinding.btnTimeRestAdd.setOnClickListener {
            mViewModel.restMinute = doAdd(mViewModel.restMinute)
            mBinding.etTimeRestMinute.setText(mViewModel.restMinute.toString())
        }

        mBinding.btnTimeRestMinus.setOnClickListener {
            mViewModel.restMinute = doMinus(mViewModel.restMinute)
            mBinding.etTimeRestMinute.setText(mViewModel.restMinute.toString())
        }

        mBinding.btnTimeRestAddSecond.setOnClickListener {
            mViewModel.restSecond = doAdd(mViewModel.restSecond)
            mBinding.etTimeRestSecond.setText(mViewModel.restSecond.toString())
        }

        mBinding.btnTimeRestMinusSecond.setOnClickListener {
            mViewModel.restSecond = doMinus(mViewModel.restSecond)
            mBinding.etTimeRestSecond.setText(mViewModel.restSecond.toString())
        }

        mBinding.btnStart.setOnClickListener {
            mViewModel.workMinute = mBinding.etTimeWorkMinute.text.toString().toInt()
            mViewModel.workSecond = mBinding.etTimeWorkSecond.text.toString().toInt()
            mViewModel.restMinute = mBinding.etTimeRestMinute.text.toString().toInt()
            mViewModel.restSecond = mBinding.etTimeRestSecond.text.toString().toInt()
            val workGap = (mViewModel.workMinute * 60 + mViewModel.workSecond) * 1000
            val restGap = (mViewModel.restMinute * 60 + mViewModel.restSecond) * 1000
            var (work, rest) = setDoubleAlarm(workGap, restGap)
            alarmManager = work.first
            workPendingIntent = work.second
            restPendingIntent = rest.second

            saveInterval(workGap, restGap)

        }

        mBinding.btnReset.setOnClickListener {
            mViewModel.workMinute = 45
            mViewModel.workSecond = 0
            mViewModel.restMinute = 15
            mViewModel.restSecond = 0
            mBinding.etTimeWorkMinute.setText(mViewModel.workMinute.toString())
            mBinding.etTimeWorkSecond.setText(mViewModel.workSecond.toString())
            mBinding.etTimeRestMinute.setText(mViewModel.restMinute.toString())
            mBinding.etTimeRestSecond.setText(mViewModel.restSecond.toString())
        }
    }

    private fun setDoubleAlarm(
        workGap: Int,
        restGap: Int
    ): Pair<Pair<AlarmManager, PendingIntent>, Pair<AlarmManager, PendingIntent>> {
        var workTime = System.currentTimeMillis() + workGap
        var restTime = workTime + restGap
        val alarmIntent = Intent(mContext, AlarmReceiver::class.java)
        var work = AlarmUtils.setExactAlarm(mContext, workTime, alarmIntent, REQUEST_CODE_WORK)
        var rest = AlarmUtils.setExactAlarm(mContext, workTime, alarmIntent, REQUEST_CODE_REST)
        return Pair(work, rest)
    }

    private fun saveInterval(workGap: Int, restGap: Int) {
        lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                DataStorePreferences.putInt(SP_KEY_WORK, workGap)
                DataStorePreferences.putInt(SP_KEY_REST, workGap)
            }
        }

    }

    private fun doAdd(data: Int): Int {
        var dataResult = data
        if (dataResult == 59) {
            dataResult = 0
        } else {
            dataResult++
        }
        return dataResult
    }

    private fun doMinus(data: Int): Int {
        var dataResult = data
        if (dataResult == 0) {
            dataResult = 59
        } else {
            dataResult--
        }
        return dataResult
    }

    override fun observeViewModel() {

    }

    override fun onNetStateChanged(connected: Boolean) {
    }


}


class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {

    }

}
