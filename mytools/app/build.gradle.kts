plugins {
    alias(libs.plugins.android.application)
    alias(libs.plugins.jetbrains.kotlin.android)
}

android {
    namespace = "com.hl.mytools"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.hl.mytools"
        minSdk = 28
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        // 脱糖处理 还要引用 coreLibraryDesugaring("com.android.tools:desugar_jdk_libs:2.0.3")
        isCoreLibraryDesugaringEnabled = true

        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }

    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }

    buildFeatures {
        viewBinding = true
        dataBinding = true
    }
}

dependencies {
    implementation(libs.androidx.browser)
    // 脱糖
    coreLibraryDesugaring(libs.desugar.jdk.libs)
    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.lifecycle.runtime.ktx)
    implementation(libs.androidx.ui)
    implementation(libs.androidx.ui.graphics)
    implementation(libs.androidx.ui.tooling.preview)
    implementation(libs.androidx.material3)
    implementation(libs.androidx.appcompat)
    implementation(libs.material)
    implementation(libs.androidx.activity)
    implementation(libs.androidx.constraintlayout)
    testImplementation(libs.junit)
    api(libs.fastjson)
    api("com.hl.lib:hllib:1.+")

    // 下拉刷新
    val smartRefreshVersion = "2.0.6"
    implementation("io.github.scwang90:refresh-layout-kernel:$smartRefreshVersion")     //核心必须依赖
    implementation("io.github.scwang90:refresh-header-classics:$smartRefreshVersion")   //经典刷新头
    implementation("io.github.scwang90:refresh-header-radar:$smartRefreshVersion")      //雷达刷新头
    implementation("io.github.scwang90:refresh-header-falsify:$smartRefreshVersion")    //虚拟刷新头
    implementation("io.github.scwang90:refresh-header-material:$smartRefreshVersion")   //谷歌刷新头
    implementation("io.github.scwang90:refresh-header-two-level:$smartRefreshVersion")  //二级刷新头
    implementation("io.github.scwang90:refresh-footer-ball:$smartRefreshVersion")       //球脉冲加载
    implementation("io.github.scwang90:refresh-footer-classics:$smartRefreshVersion")   //经典加载

    //RecyclerView的封装
    implementation("com.github.liangjingkanji:BRV:1.5.8")
    implementation("com.github.liangjingkanji:StatusBar:2.0.5")

    val dialogx_version = "0.0.49"
    implementation("com.github.kongzue.DialogX:DialogX:${dialogx_version}")

    // 应用内打开网址
    implementation("com.google.android.material:material:1.4.0")

    
}