package com.hl.mytools.database

import com.hl.lib.utils.ContextUtil
import com.hl.mytools.ToolApplication

object FundDBHelper {

    suspend fun insertFund(fundInfo: FundInfo){
        val database = FundDataBase.instance
        database.fundInfoDao().insert(fundInfo)
    }

}