package com.hl.mytools

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.hl.lib.utils.ContextUtil
import com.hl.mytools.database.FundDataBase


class ToolApplication : Application() {

    companion object {
        private const val TAG = "ToolApplication"
    }

    override fun onCreate() {
        super.onCreate()
        ContextUtil.init(applicationContext)
    }


}