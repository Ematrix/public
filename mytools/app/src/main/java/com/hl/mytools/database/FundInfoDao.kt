package com.hl.mytools.database

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

interface  FundInfoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(fundInfo: FundInfo)

    @Update
    suspend fun update(fundInfo: FundInfo)

    @Delete
    suspend fun delete(fundInfo: FundInfo)

    @Query("SELECT * FROM FundInfo WHERE code = :fundCode")
    suspend fun getFundInfoByCode(fundCode: String): FundInfo?

    @Query("SELECT * FROM FundInfo")
    suspend fun getAllFundInfoList(): List<FundInfo>
}