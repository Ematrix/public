package com.hl.mytools.entity;

/**
 * @author hyhdez
 * @date 2022/8/7 14:21
 * @since
 */
public class FundPriceInfo {
    /**
     * 日期
     */
    private String fbrq;
    /**
     * 单位净值
     */
    private String jjjz;
    /**
     * 累计净值
     */
    private String ljjz;

    public FundPriceInfo(String fbrq, String jjjz, String ljjz) {
        this.fbrq = fbrq;
        this.jjjz = jjjz;
        this.ljjz = ljjz;
    }

    public FundPriceInfo() {
    }

    public String getFbrq() {
        return fbrq;
    }

    public void setFbrq(String fbrq) {
        this.fbrq = fbrq;
    }

    public String getJjjz() {
        return jjjz;
    }

    public void setJjjz(String jjjz) {
        this.jjjz = jjjz;
    }

    public String getLjjz() {
        return ljjz;
    }

    public void setLjjz(String ljjz) {
        this.ljjz = ljjz;
    }

    @Override
    public String toString() {
        return "FundInfo{" +
                "fbrq='" + fbrq + '\'' +
                ", jjjz='" + jjjz + '\'' +
                ", ljjz='" + ljjz + '\'' +
                '}';
    }
}
