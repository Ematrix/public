package com.hl.mytools.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.hl.lib.utils.ContextUtil

@Database(entities = [FundInfo::class, PriceInfo::class], version = 1)
abstract class FundDataBase : RoomDatabase() {

    companion object {
        private val FUND_DATABASE_NAME = "fund_database"
        val instance =
            Room.databaseBuilder(ContextUtil.get(), FundDataBase::class.java, FUND_DATABASE_NAME)
                .build()
    }

    abstract fun fundInfoDao(): FundInfoDao
    abstract fun priceInfoDao(): PriceInfoDao

}