package com.hl.mytools.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.hl.mytools.entity.FundEntity
import com.hl.mytools.entity.FundPriceInfo

@Entity(tableName = "FundInfo")
data class FundInfo(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long = 0,
    @ColumnInfo(name = "code")
    var code: String,
    @ColumnInfo(name = "name")
    var name: String,
    @Ignore
    var priceData: FundEntity? = null,
    @Ignore
    var fundPriceInfoList: MutableList<FundPriceInfo>? = null,

    // 创新高,创新低 等等
    @Ignore
    var adviceReason: String = "",

    // 买,卖
    @Ignore
    var adviceKind: String = "",

    @Ignore
    var trade: String = "",

    @Ignore
    var source: String = "",
    )