package com.hl.mytools.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "priceInfo",
    foreignKeys = [
        ForeignKey(
            entity = FundInfo::class,
            parentColumns = ["code"],
            childColumns = ["code"],
            onDelete = ForeignKey.CASCADE
        )
    ],
    indices = [Index("code")] // 为外键创建索引以优化查询)
)
data class PriceInfo(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Long = 0,
    @ColumnInfo(name = "code")
    var code: String,
    @ColumnInfo(name = "date")
    var data: String,
    @ColumnInfo(name = "price")
    var price: String,
)