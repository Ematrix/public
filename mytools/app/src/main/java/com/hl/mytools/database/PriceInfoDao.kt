package com.hl.mytools.database

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

interface PriceInfoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(priceHistory: PriceInfo)

    @Update
    suspend fun update(priceHistory: PriceInfo)

    @Delete
    suspend fun delete(priceHistory: PriceInfo)

    // 查询特定基金代码在某日期的价格
    @Query("SELECT * FROM PriceInfo WHERE code = :fundCode AND date = :date")
    suspend fun getPriceInfoByCodeAndDate(fundCode: String, date: String): PriceInfo?

    // 获取某个基金的所有价格历史
    @Query("SELECT * FROM PriceInfo WHERE code = :fundCode ORDER BY date")
    suspend fun getPriceInfoByCode(fundCode: String): List<PriceInfo>

    // 获取所有价格历史记录
    @Query("SELECT * FROM PriceInfo")
    suspend fun getAllPriceInfoList(): List<PriceInfo>


    @Query("""
        SELECT FundInfo.name,PriceInfo.date,PriceInfo.price
        FROM FundInfo
        INNER JOIN PriceInfo ON FundInfo.code = PriceInfo.code
        WHERE FundInfo.code = :fundCode
        ORDER BY PriceInfo.date DESC
        LIMIT 5
    """)
    suspend fun getLastPriceAndNameByFundCode(fundCode: String): Pair<String?, Double?>

}