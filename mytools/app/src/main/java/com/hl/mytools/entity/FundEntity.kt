

package com.hl.mytools.entity

data class FundEntity (
    val result: Result? = null
)

data class Result (
    val data: Data?,
    val status: Status?
)

data class Data (
    val data: List<Datum>?,
    val totalNum: String?
)

data class Datum (
    val jjjz: String?,
    val fbrq: String?,
    val ljjz: String?
)

data class Status (
    val code: Long?
)
