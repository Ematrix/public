package com.hl.mytools.data

 object Urls {

     // 基金的热门主题
     const val hot_Subject =
//         "https://fund.eastmoney.com/ztjj/#!c/001002/curr/zf-%E7%83%AD%E9%97%A8%E4%B8%BB%E9%A2%98"
         "https://fund.eastmoney.com/ztjj/#!c/001002/curr/BK000076-%E7%99%BD%E9%85%92/fst/DESC"

     //基金净值查询
     const val fund_query_url = "https://stock.finance.sina.com.cn/fundInfo/api/openapi.php/CaihuiFundInfoService.getNav?callback=jQuery111209251066698798494_1659844062464&symbol="

     //基金详情
     const val tiantian_fund_detail_url = "https://h5.1234567.com.cn/app/fund-details/?fCode="
     const val tonghua_fund_detail_url = "https://fund.10jqka.com.cn/public/ifundout/dist/detail.html#/"
}