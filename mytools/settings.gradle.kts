pluginManagement {
    repositories {
        maven { url = uri("file://D:/localMaven") }
        maven { url = uri("file://D:/localMaven/") }
        maven { url = uri("file://D:/localMaven/local-maven-repo/") }
        mavenLocal() // 使用默认的本地仓库
        maven { url = uri("https://maven.aliyun.com/repository/public/") }
        maven { url = uri("https://maven.aliyun.com/repository/google/") }
        maven { url = uri("https://maven.aliyun.com/repository/jcenter/") }
        maven { url = uri("https://maven.aliyun.com/repository/central/") }
        maven { url = uri("https://jitpack.io") }
        google {
            content {
                includeGroupByRegex("com\\.android.*")
                includeGroupByRegex("com\\.google.*")
                includeGroupByRegex("androidx.*")
            }
        }
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)

    repositories {
        maven { url = uri("file://D:/localMaven") }
        maven { url = uri("file://D:/localMaven/") }
        maven { url = uri("file://D:/localMaven/local-maven-repo/") }
        mavenLocal() // 使用默认的本地仓库
        maven { url = uri("https://maven.aliyun.com/repository/public/") }
        maven { url = uri("https://maven.aliyun.com/repository/google/") }
        maven { url = uri("https://maven.aliyun.com/repository/jcenter/") }
        maven { url = uri("https://maven.aliyun.com/repository/central/") }
        maven { url = uri("https://jitpack.io") }
        google()
        mavenCentral()
    }
}

rootProject.name = "mytools"
include(":app")
