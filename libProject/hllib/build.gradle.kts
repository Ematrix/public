import java.time.Instant

plugins {
//    alias(libs.plugins.android.library)
//    alias(libs.plugins.jetbrains.kotlin.android)

    id("com.android.library") // 对于库模块
//    id("com.android.application") // 对于应用模块
    id("kotlin-android") // 如果项目使用 Kotlin
    id("kotlin-kapt") // 如果项目使用 Kotlin 注解处理器（KAPT）
    id("maven-publish")
}

android {
    namespace = "com.hl.lib"
    compileSdk = 34

    defaultConfig {
//        applicationId = "com.hl.lib"
        minSdk = 28
//        versionCode = 1
//        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release")  {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }

        getByName("debug")  {
            isMinifyEnabled = false
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }

    compileOptions {
        // 脱糖处理 还要引用 coreLibraryDesugaring("com.android.tools:desugar_jdk_libs:2.0.3")
        isCoreLibraryDesugaringEnabled = true

        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    buildFeatures {
        viewBinding = true
        dataBinding = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.5.1"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }

//    // 虽然通常不需要显式声明main sourceSet，但如果你想自定义可以这么做
//    sourceSets {
//        // 'main' SourceSet 已经由Kotlin插件默认创建，这里展示如何访问和潜在的自定义
//        named("main") {
//            // 添加额外的编译选项、源目录等
//            // compileClasspath += files(...) // 添加到编译类路径
//            // java.srcDirs("src/main/myCustomJavaDir") // 添加额外的Java源目录
//            // resources.srcDirs("src/main/myCustomResourcesDir") // 添加额外的资源目录
//        }
//
//        // 对于test源集，如果需要自定义
//        named("test") {
//            // 自定义配置
//        }
//    }

}

val mainVersion = "1"
val timestampBasedVersion: () -> String = {
    val now = Instant.now()
    val epochSeconds = now.epochSecond
    println("到目前为止的秒数 : $epochSeconds")
    // 舍弃后两位,即按百秒算,两次打包的时间间隔应该大于100秒
    val temp = epochSeconds / 100
    println("到目前为止的秒数/100 : $temp")
    val result = "${temp / 10000}.${temp % 10000}"
    println(result)
    result
}

group = "com.hl.lib" // 组名，类似于 Java 的包名
version = "${mainVersion}.${timestampBasedVersion()}"

java {
//    withJavadocJar() // 打包时包含javadoc
//    withSourcesJar() // 打包时包含源代码
}


afterEvaluate{
    publishing {
        publications {
            create<MavenPublication>("debug") {
                groupId = group.toString()
                artifactId = project.name
                version = version

                // 从组件中获取 artifacts，默认为 release 版本
                from(components["debug"])

                // 如果需要，可以进一步定制 POM 文件信息
                // 添加javadoc和sources jar
//                artifact(tasks.named<Jar>("javadocJar"))
//                artifact(tasks.named<Jar>("sourcesJar"))

            }
        }

        repositories {
            maven {
                name = "localMaven"
                url = uri("D:/localMaven/local-maven-repo") // 替换为你的本地仓库路径
            }
        }
    }
}


dependencies {

    // 脱糖
    coreLibraryDesugaring(libs.desugar.jdk.libs)

    api(libs.androidx.core.ktx)
    api(libs.androidx.lifecycle.runtime.ktx)
    api(libs.androidx.activity.compose)
    api(platform(libs.androidx.compose.bom))
    api(libs.androidx.ui)
    api(libs.androidx.ui.graphics)
    api(libs.androidx.ui.tooling.preview)
    api(libs.androidx.material3)
    api(libs.androidx.appcompat)
    api(libs.material)
    api(libs.androidx.activity)
    api(libs.androidx.constraintlayout)
    api(libs.androidx.navigation.fragment.ktx)
    api(libs.androidx.navigation.ui.ktx)
    api(libs.androidx.lifecycle.viewmodel.compose)
    implementation(libs.androidx.browser)
    implementation(libs.androidx.window)
    testImplementation(libs.junit)
    androidTestImplementation(libs.androidx.junit)
    androidTestImplementation(libs.androidx.espresso.core)
    androidTestImplementation(platform(libs.androidx.compose.bom))
    androidTestImplementation(libs.androidx.ui.test.junit4)
    debugImplementation(libs.androidx.ui.tooling)
    debugImplementation(libs.androidx.ui.test.manifest)

//    api(libs.glide)
//    kapt(libs.compiler)

    api(libs.okhttp3.integration)
    api(libs.okhttp)
    api(libs.work.runtime.ktx)
    api(libs.kotlinx.coroutines.core)
    api(libs.kotlinx.coroutines.android)
    api(libs.fastjson)
    api(libs.material.dialogs.core)
//    api(libs.material.dialogs.commons)
    api(libs.material.dialogs.datetime)
    api(libs.material.dialogs.files)
    api(libs.material.dialogs.lifecycle)

    val room_version = "2.6.1"
    api("androidx.room:room-runtime:$room_version")
    api("androidx.room:room-ktx:$room_version")
    kapt("androidx.room:room-compiler:$room_version")
    testImplementation("androidx.room:room-testing:$room_version")

    api("androidx.datastore:datastore-preferences:1.0.0")

    val dialogx_version = "0.0.49"
    api("com.github.kongzue.DialogX:DialogX:${dialogx_version}")

    api(libs.androidsvg.aar)


}

