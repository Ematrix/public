package com.hl.lib.utils

import android.annotation.SuppressLint
import android.content.Context


@SuppressLint("StaticFieldLeak")
object ContextUtil {
    private lateinit var mContext:Context
    fun init(context: Context) {
        mContext = context
    }

    fun get():Context{
        return mContext
    }

}