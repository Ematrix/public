package com.hl.lib.base

import android.app.Application

val appContext = BaseApplication.context

open class BaseApplication : Application() {

    companion object { lateinit var context: Application }

    override fun onCreate() {
        super.onCreate()
        context = this
    }
}