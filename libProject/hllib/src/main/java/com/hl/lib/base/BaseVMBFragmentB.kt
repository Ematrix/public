package com.hl.lib.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

/**
 * 封装了ViewModel和DataBinding的Fragment基类
 *
 * @author LTP  2021/11/23
 */
abstract class BaseVMBFragmentB<VB : ViewBinding> : Fragment() {

    val TAG = this.javaClass.simpleName

    protected lateinit var mBinding: VB

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = provideViewBinding(inflater,container)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initData()
        initView()
    }

    /**
     * 子类需要实现这个方法，返回对应的ViewBinding实例
     */
    protected abstract fun provideViewBinding(inflater: LayoutInflater, container: ViewGroup?): VB

    protected abstract fun initData()
    protected abstract fun initView()


}