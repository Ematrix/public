package com.hl.lib.base

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import java.lang.reflect.ParameterizedType

/**
 * 封装了ViewModel和DataBinding的Fragment基类
 *
 * @author LTP  2021/11/23
 */
abstract class BaseVMBFragmentBM<VB : ViewBinding, VM : BaseViewModel> : BaseVMBFragmentB<VB>() {


    protected abstract val viewModel: VM

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
    }

    abstract fun observeViewModel()
}


/**
 * class MyFragment : BaseFragment<MyFragmentBinding, MyFragmentViewModel>() {
 *
 *     override val viewModel: MyFragmentViewModel by viewModels()
 *
 *     override fun provideViewBinding(): MyFragmentBinding {
 *         return MyFragmentBinding.inflate(layoutInflater, container, false)
 *     }
 *
 *     override fun observeViewModel() {
 *         viewModel.someLiveData.observe(viewLifecycleOwner) { data ->
 *             // 更新 UI 或处理数据
 *         }
 *     }
 * }
 */