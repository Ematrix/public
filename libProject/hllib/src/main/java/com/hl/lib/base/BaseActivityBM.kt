package com.hl.lib.base

import android.os.Bundle
import androidx.viewbinding.ViewBinding

abstract class BaseActivityBM<VB:ViewBinding,VM:BaseViewModel>: BaseActivityB<VB>() {

    protected abstract val mViewModel: VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeViewModel()
    }

    abstract fun observeViewModel()
}


/**
 * class MyActivity : BaseActivity<MyActivityBinding, MyViewModel>() {
 *
 *     override val viewModel: MyViewModel by viewModels()
 *
 *     override fun provideViewBinding(): MyActivityBinding {
 *         return MyActivityBinding.inflate(layoutInflater)
 *     }
 *
 *     override fun observeViewModel() {
 *         viewModel.someLiveData.observe(this) { data ->
 *             // 更新 UI 或处理数据
 *         }
 *     }
 * }
 */