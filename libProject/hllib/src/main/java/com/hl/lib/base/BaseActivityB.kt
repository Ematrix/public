package com.hl.lib.base

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Bundle
import android.util.Log
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.alibaba.fastjson.JSON
import com.kongzue.dialogx.dialogs.WaitDialog

abstract class BaseActivityB<VB : ViewBinding> : AppCompatActivity() {

    val TAG = this.javaClass.simpleName

    protected lateinit var mBinding: VB
    protected val mContext = this
    private var connectivityManager: ConnectivityManager? = null
    private var isNetworkCallbackRegistered = false

    val request = NetworkRequest.Builder()
        .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
        .build()

    private val callback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            onNetStateChanged(true)
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            onNetStateChanged(false)
        }
    }

    private  val activityResultLauncher  by lazy{
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            result?.let {
                Log.e(TAG, JSON.toJSONString(it))
                it.data?.let {data ->
                    Log.e(TAG, JSON.toJSONString(data))
                    doOnActivityResult(data)
                }
            }
        }
    }

    companion object {
        fun launchActivity(context: Context, bundle: Bundle? = null) {
            val intent = Intent(context, this::class.java).apply {
                bundle?.let {
                    putExtras(it)
                }
            }
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // 初始化ViewBinding
        mBinding = provideViewBinding()
        setContentView(mBinding.root)
        initData()
        initView()
    }

    override fun onResume() {
        super.onResume()
        if (connectivityManager == null) {
            connectivityManager =
                getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        }


        connectivityManager?.registerNetworkCallback(request, callback)
        isNetworkCallbackRegistered = true

    }

    override fun onPause() {
        if (isNetworkCallbackRegistered) {
            connectivityManager?.unregisterNetworkCallback(callback)
        }
        super.onPause()
    }

    fun showLoading(msg:String) {
        WaitDialog.show(msg);
    }

    fun dismissLoading() {
        WaitDialog.dismiss()
    }

    /**
     * 子类需要实现这个方法，返回对应的ViewBinding实例
     */
    protected abstract fun provideViewBinding(): VB
    protected abstract fun initData()
    protected abstract fun initView()
    protected abstract fun onNetStateChanged(connected: Boolean)

    protected fun doOnActivityResult(data: Intent){

    }
}