package com.hl.lib.utils.net;

/**
 * @author hyhdez
 * @des
 * @date 2022/8/7 12:58
 */
public interface NetCallback {
    /**
     * 成功
     *
     * @param code   code
     * @param result result
     */
    public void onSuccess(int code, String result);

    /**
     * 失败
     *
     * @param errorCode errorCode
     * @param message   message
     */
    public void onFail(int errorCode, String message);
}
