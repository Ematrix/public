package com.hl.lib.ext

import android.annotation.SuppressLint
import android.text.Html
import android.text.Spanned
import android.util.Base64
import java.math.BigInteger
import java.security.MessageDigest

/**
 * String扩展类
 * @author LTP  2022/3/25
 */
fun String.toHtml(@SuppressLint("InlinedApi") flag: Int = Html.FROM_HTML_MODE_LEGACY): Spanned {
    return if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
        Html.fromHtml(this, flag)
    } else {
        Html.fromHtml(this)
    }
}


object MimeType {

    const val ANY = "*/*"
    const val FONT = "font/ttf"
    const val OPML = "text/x-opml"  // Not supported yet
    const val JSON = "application/json"
}

fun String.formatUrl(): String {
    if (this.startsWith("//")) {
        return "https:$this"
    }
    val regex = Regex("^(https?|ftp|file).*")
    return if (!regex.matches(this)) {
        "https://$this"
    } else {
        this
    }
}

fun String.isUrl(): Boolean {
    val regex = Regex("(https?|ftp|file)://[-A-Za-z0-9+&@#/%?=~_|!:,.;]+[-A-Za-z0-9+&@#/%=~_|]")
    return regex.matches(this)
}

fun String.mask(): String = run {
    "\u2022".repeat(length)
}

fun String.encodeBase64(): String = Base64.encodeToString(toByteArray(), Base64.DEFAULT)

fun String.decodeBase64(): String = String(Base64.decode(this, Base64.DEFAULT))

fun String.md5(): String =
    BigInteger(1, MessageDigest.getInstance("MD5").digest(toByteArray()))
        .toString(16).padStart(32, '0')

fun String?.decodeHTML(): String? = this?.run { Html.fromHtml(this).toString() }

fun String?.orNotEmpty(l: (value: String) -> String): String =
    if (this.isNullOrBlank()) "" else l(this)
