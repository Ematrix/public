package com.hl.lib.const

import java.time.format.DateTimeFormatter

object DateFormats {
    val normalDataTime = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
    val normalData = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    val normalTime = DateTimeFormatter.ofPattern("HH:mm:ss")

}