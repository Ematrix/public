package com.hl.lib.utils.stackUtils

object StackTraceUtils {
    fun print() {
        val currentThread = Thread.currentThread()
        for (element in currentThread.stackTrace) {
            println("at ${element.className}.${element.methodName}(${element.fileName}:${element.lineNumber})")
        }
    }
}