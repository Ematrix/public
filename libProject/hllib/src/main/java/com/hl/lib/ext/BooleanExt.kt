package com.hl.lib.ext

fun Boolean.toInt(): Int = if (this) 1 else 0
