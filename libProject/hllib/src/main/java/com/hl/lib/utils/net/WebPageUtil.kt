package com.hl.lib.utils.net

import android.content.Context
import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent

object WebPageUtil {

     fun openUrl(context: Context, url: String) {
         val builder = CustomTabsIntent.Builder()
//         if (url.startsWith("http://") || url.startsWith("https://")) {
//             builder.setShowTitle(true)
//         }
         // 可以在这里设置更多的自定义选项，比如颜色、分享状态栏等
         // builder.setStartAnimations(context, R.anim.slide_in_right, R.anim.slide_out_left)
         // builder.setExitAnimations(context, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
         val customTabsIntent = builder.build()
         customTabsIntent.launchUrl(context, Uri.parse(url))
     }

}