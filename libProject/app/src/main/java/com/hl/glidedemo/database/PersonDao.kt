package com.hl.glidedemo.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

@Dao
interface PersonDao {

    @Delete
    suspend fun delete(person: Person)

    @Update
    suspend fun update(person: Person)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(person: Person)

    @Query("SELECT * FROM person WHERE id = :id")
    suspend fun getUserById(id: Long): Person


    @Query("SELECT * FROM person WHERE age > :age")
    suspend fun getPersonsOlderThan(age: Int): List<Person>
}
