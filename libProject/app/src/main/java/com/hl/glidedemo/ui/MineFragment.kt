package com.hl.glidedemo.ui

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.hl.glidedemo.R
import com.hl.glidedemo.databinding.FragmentMineBinding
import com.hl.lib.base.BaseVMBFragmentBM

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class MineFragment : BaseVMBFragmentBM<FragmentMineBinding, MineFragmentModel>() {
    private var param1: String? = null
    private var param2: String? = null
    override val viewModel: MineFragmentModel by viewModels()

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            MineFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.e(TAG, "onCreate: Enter")
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun observeViewModel() {

    }

    override fun provideViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentMineBinding {
        Log.e(TAG, "provideViewBinding: ")
        return FragmentMineBinding.inflate(layoutInflater, container, false)
    }

    override fun initData() {
        
    }

    override fun initView() {
        Log.e(TAG, "initView: ")
    }
    
    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        Log.e(TAG, "onConfigurationChanged: start")
//        requireActivity().supportFragmentManager
//            .beginTransaction()
//            .remove(this)
//            .add(R.id.flContainer,this)
//            .commitNow()

        val newBinding = FragmentMineBinding.inflate(layoutInflater)

        val parent: ViewGroup = mBinding.getRoot().parent as ViewGroup
        parent?.let {
            parent.removeView(mBinding.root)
            parent.addView(newBinding.root)
        }

        Log.e(TAG, "onConfigurationChanged: end")
    }
}