package com.hl.glidedemo.bitmaptarget

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import com.bumptech.glide.request.Request
import com.bumptech.glide.request.target.SizeReadyCallback
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition

class BitmapTarget(private val callback: (Bitmap?) -> Unit) : Target<Bitmap>{
    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
        callback(resource)
    }

    // 可选：如果需要在加载失败时收到通知，可以重写以下方法
    override fun onLoadFailed(errorDrawable: Drawable?) {
        callback(null) // 或者其他失败处理逻辑
    }

    override fun onLoadCleared(placeholder: Drawable?) {}
    override fun getSize(cb: SizeReadyCallback) {
        
    }

    override fun removeCallback(cb: SizeReadyCallback) {
        
    }

    override fun setRequest(request: Request?) {
        
    }

    override fun getRequest(): Request? {
        return null
    }

    override fun onStart() {
        
    }

    override fun onStop() {
        
    }

    override fun onDestroy() {
        
    }

    override fun onLoadStarted(placeholder: Drawable?) {
        
    }


}