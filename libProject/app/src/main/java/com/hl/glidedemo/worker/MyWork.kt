package com.hl.glidedemo.worker

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine

class MyWork(context: Context, workerParams: WorkerParameters) : Worker(context, workerParams) {

    companion object {
        private const val TAG = "MyWork"
        private const val imageUrl =
            "https://www.25h.net/d/file/20230115/b1dd4ed72f4ebdb7a9c5dade1df5a08d.jpg"

        private const val imageUrl2 =
            "https://file1.shop265.com/tk/20200809/02330d1ddcc43d76b78aeb544f69ea48.jpg"
    }

    val coroutineScope = CoroutineScope(Dispatchers.IO)


    override fun doWork(): Result {
        var bitmap1: Bitmap? = null
        var bitmap2: Bitmap? = null
        val launch = coroutineScope.async(Dispatchers.IO) {
            bitmap1 = getBitmapFromGlide(imageUrl)
            Log.e(TAG, "doWork: 得到 bitmap1 ")
        }

        val launch2 = coroutineScope.async(Dispatchers.IO) {
            bitmap2 = getBitmapFromGlide(imageUrl2)
            Log.e(TAG, "doWork: 得到 bitmap2 ")
        }

        coroutineScope.launch {
            awaitAll(launch, launch2)
            Log.e(TAG, "doWork: 两个bitmap都已获得 $bitmap1")
            Log.e(TAG, "doWork: 两个bitmap都已获得 $bitmap2")
        }

        return Result.success()
    }


    private suspend fun getBitmapFromGlide(url: String): Bitmap? {
        return suspendCancellableCoroutine { continuation ->
            Glide.with(applicationContext)
                .asBitmap()
                .load(url)
                .listener(object : RequestListener<Bitmap> {
                    override fun onResourceReady(
                        resource: Bitmap,
                        model: Any,
                        target: Target<Bitmap>?,
                        dataSource: DataSource,
                        isFirstResource: Boolean
                    ): Boolean {
                        continuation.resumeWith(kotlin.Result.success(resource))
                        return false
                    }

                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Bitmap>,
                        isFirstResource: Boolean
                    ): Boolean {
                        continuation.resumeWith(
                            kotlin.Result.failure(
                                e ?: Exception("Unknown Glide error")
                            )
                        )
                        return false
                    }
                })
                .submit()
        }
    }


}


